package com.fearthe1337.fearutil.configuration;

public @interface TupilTypeAnnotation {
	Class<?> keyType();
	Class<?> valueType();
}
