package com.fearthe1337.fearutil.impl;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.fearthe1337.fearutil.checks.ArgumentCheck;
import com.fearthe1337.fearutil.configuration.AutoConfiguration;
import com.fearthe1337.fearutil.configuration.ConfigValue;
import com.fearthe1337.fearutil.configuration.ConfigurationHandler;
import com.fearthe1337.fearutil.configuration.ConfigurationObject;
import com.fearthe1337.fearutil.reflection.ReflectionUtil;

public class BukkitConfigurationHandler<T extends AutoConfiguration> extends
		ConfigurationHandler<T> {
	static HashSet<Class<?>> defaultTypes;
	private FileConfiguration _fc;
	private Class<T> clazz;
	private Object[] parms;

	public BukkitConfigurationHandler(Class<T> clazz, FileConfiguration fc,
			Object[] parms) {
		ArgumentCheck.notNULL(clazz, "Passed class may not be NULL!");
		ArgumentCheck.notNULL(fc,
				"Passed FileConfiguration class may not be NULL!");
		this._fc = fc;
		this.clazz = clazz;
		this.parms = parms;
	}

	public T load() {
		try {
			return handleLoadClass(_fc.getValues(true), clazz);
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}

	@SuppressWarnings("unchecked")
	private Object handleLoadArray(Object val, Class<?> clazz) throws Exception{
		Class<?> type = clazz.getComponentType();
		//Multi dimensional array handling
		if(type.isArray()){
			if(val instanceof List){
				List<?> elements = (List<?>)val;
				Object arr = Array.newInstance(type, elements.size());
				for(int i=0; i < elements.size(); i++){
					Object newval = elements.get(i);
					Object o = handleLoadArray(newval,type);
					Array.set(arr, i, o);
				}
			}else{
				throw new RuntimeException("Array misconfiguration.");
			}
		}
		
		if(val instanceof List){
			List<?> elements = (List<?>)val;
			Object arr = Array.newInstance(type, elements.size());
			for(int i=0; i < elements.size(); i++){
				Object newval = elements.get(i);
				if(newval instanceof Map){//Were dealing with an object type, not a default type (string,int..)
					Map<String,Object> newmap = (Map<String,Object>)newval;
					newval = handleLoadClass(newmap,type);
				}
				Array.set(arr, i, newval);
			}
			return arr;
		}else{
			throw new RuntimeException("Error handling array, Class mismatch " + val + "/" + val.getClass() + " vs " + type);
		}
	}
	//double/float
	private Object basicTypeCast(Object value, Class<?> wanted){
		if(value instanceof Number){
			Integer number = (Integer)value;
			if(wanted.equals(Byte.class) || wanted.equals(Byte.TYPE)){
				return number.byteValue();
			}else if(wanted.equals(Double.class) || wanted.equals(Double.TYPE)){
				return number.doubleValue();
			}else if(wanted.equals(Float.class) || wanted.equals(Float.TYPE)){
				return number.floatValue();
			}else if(wanted.equals(Integer.class) || wanted.equals(Integer.TYPE)){
				return number.intValue();
			}else if(wanted.equals(Long.class) || wanted.equals(Long.TYPE)){
				return number.longValue();
			}else if(wanted.equals(Short.class) || wanted.equals(Short.TYPE)){
				return number.shortValue();
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private <U> U handleLoadClass(Map<String,Object> map, Class<U> clazz) throws Exception {
		Constructor<U> constructor = ReflectionUtil.resolveConstructor(clazz, parms);
		U configObject = null;
		
		if(constructor.isAccessible()){
			configObject = constructor.newInstance(parms);
		}else{
			constructor.setAccessible(true);
			configObject = constructor.newInstance(parms);
			constructor.setAccessible(false);
		}
		
		List<Field> settings = ReflectionUtil.getFields(configObject.getClass(),ConfigValue.class);
		for(Field f : settings){
			Class<?> type = f.getType();
			ConfigValue annotation = f.getAnnotation(ConfigValue.class);
			Object value = map.get(annotation.key());
			if(value == null){
				if(annotation.setNull()){
					ReflectionUtil.setField(f, configObject, null);
				}
				continue;
			}
			if(type.isArray()){
				Object obj = handleLoadArray(value,type);
				ReflectionUtil.setField(f, configObject, obj);
			}else{
				if(value.getClass() == type || ReflectionUtil.translatePrimitive(type) == value.getClass()){
					ReflectionUtil.setField(f, configObject, value);
				}else{
					if(type.isAnnotationPresent(ConfigurationObject.class)){
						if(value instanceof Map){
							Map<String,Object> newMap = (Map<String,Object>)value;
							Object obj = handleLoadClass(newMap,f.getType());
							ReflectionUtil.setField(f, configObject, obj);
						}else{
							throw new RuntimeException("Invalid configured key: " + annotation.key());
						}
					}else{
						Object converted = basicTypeCast(value,type);
						if(converted != null){
							ReflectionUtil.setField(f, configObject, converted);
						}else{
							throw new RuntimeException("Invalid configured key: " + annotation.key() + "Expected: " + type + " got: " + value.getClass());
						}
					}
				}
			}
		}
		return (U) configObject;
	}
	
	private List<Object> handleSaveArray(Object arr){
		Class<?> type = arr.getClass().getComponentType();
		int length = Array.getLength(arr);
		//Multi dimensional array handling
		if(type.isArray()){
			ArrayList<Object> list = new ArrayList<Object>();
			for(int i=0; i < length; i++){
				Object value = Array.get(arr, i);
				if(value == null)
					continue;
				Object raw = handleSaveArray(value);
				list.add(raw);
			}
			return list;
		}
		
		//single dimension array handling.
		if(type.isAnnotationPresent(ConfigurationObject.class)){
			ArrayList<Object> list = new ArrayList<Object>();
			for(int i=0; i < length; i++){
				Object value = Array.get(arr, i);
				if(value == null)
					continue;
				Object raw = handleSaveClass(value);
				list.add(raw);
			}
			return list;
		}else{
			ArrayList<Object> list = new ArrayList<Object>();
			for(int i=0; i < length; i++){
				Object value = Array.get(arr, i);
				if(value == null)
					continue;
				list.add(value);
			}
			return list;
		}
	}
	
	private boolean isDefaultType(Class<?> clazz){
		return defaultTypes.contains(clazz) || (ReflectionUtil.translatePrimitive(clazz) != null) || (Number.class.isAssignableFrom(clazz));
	}
	
	
	private Map<String,Object> handleSaveClass(Object config){
		HashMap<String,Object> map = new HashMap<String,Object>();
		
		List<Field> settings = ReflectionUtil.getFields(config.getClass(),ConfigValue.class);
		for(Field f : settings){
			Object value = null;
			Class<?> type = f.getType();
			ConfigValue annotation = f.getAnnotation(ConfigValue.class);
			Object raw = ReflectionUtil.getField(f, config);
			
			if(raw != null && !type.isArray()){
				if(type.isAnnotationPresent(ConfigurationObject.class)){
						value = handleSaveClass(raw);
						map.put(annotation.key(),value);
				}else{
					if(isDefaultType(type)){
						map.put(annotation.key(),raw);
					}else{
						throw new RuntimeException("Dont know how to handle field: " + annotation.key());
					}
				}
			}else if(raw != null && type.isArray()){
				value = handleSaveArray(raw);
				map.put(annotation.key(),value);
			}
		}
		return map;
	}
	
	
	
	private void setData(FileConfiguration fc, Map<String,Object> values){
		//Clear current config.
		Set<String> keys = fc.getKeys(false);
		if(keys != null){
			for(String key : keys){
				fc.set(key, null);
			}
		}
		
		if(values == null){
			return;
		}
		
		//Copy our settings in portions.
		Set<Entry<String,Object>> sections = values.entrySet();
		if(sections == null){
			return;
		}
		
		for(Entry<String,Object> section : sections){
			fc.set(section.getKey(), section.getValue());
		}
	}

	/** Attempts to save the configuration.
	 * If the handler supplied is a file, this function will attempts to 
	 * save the data into the file.
	 * 
	 * If the handler specified is a null value, the function will attempt to save the data
	 * into the FileConfiguration specified at the constructor.
	 * 
	 * If the handler specified is a instanceof FileConfiguration, the function will attempt to save the data
	 * into the specified FileConfiguration.
	 * 
	 * Note: If a non-file is specified, the data will only be loaded into the FileConfiguration.
	 * If you wish to save the data use Plugin.saveConfig or call the save function on the supplied FileConfiguration.
	 * @throws IOException 
	 * 
	 * @see com.fearthe1337.fearutil.configuration.ConfigurationHandler#save(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void save(T configuration, Object handler) throws IOException {
		Map<String,Object> values = this.handleSaveClass(configuration);
		FileConfiguration fc;
		if(handler == null){
			fc = _fc;
		}if(handler instanceof File){
			fc = YamlConfiguration.loadConfiguration((File)handler);
			setData(fc,values);
			fc.save((File)handler);
			return;
		}else if(handler instanceof FileConfiguration){
			fc = (FileConfiguration)handler;
		}else{
			throw new RuntimeException("Unknown configuration storage: " + handler.getClass());
		}
		setData(fc,values);
	}
	
	static{
		defaultTypes = new HashSet<Class<?>>();
		defaultTypes.add(Boolean.class);
		defaultTypes.add(Integer.class);
		defaultTypes.add(String.class);
	}
}
