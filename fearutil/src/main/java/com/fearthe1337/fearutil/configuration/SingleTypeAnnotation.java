package com.fearthe1337.fearutil.configuration;

public @interface SingleTypeAnnotation {
	Class<?> keyType();
}
