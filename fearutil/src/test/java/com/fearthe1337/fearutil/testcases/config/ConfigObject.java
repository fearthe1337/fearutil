package com.fearthe1337.fearutil.testcases.config;

import com.fearthe1337.fearutil.configuration.AutoConfiguration;
import com.fearthe1337.fearutil.configuration.ConfigValue;
import com.fearthe1337.fearutil.configuration.ConfigurationObject;

public class ConfigObject extends AutoConfiguration{
	@ConfigValue(key = "teststr")
	public String testString;
	
	@ConfigValue(key = "array")
	public TestArrayClass[] keys;

	@Override
	public void validate() {
		
	}
	
	@ConfigurationObject
	static class TestArrayClass{
		@ConfigValue(key = "id")
		private int demoValue = 1;
		@ConfigValue(key = "value")
		private String demoStr = "";
		@ConfigValue(key = "short")
		private short test3 = -1;
		
		public TestArrayClass(){}
		public TestArrayClass(int value, String str){
			this.demoValue = value;
			this.demoStr = str;
		}
		
		@Override
		public boolean equals(Object other){
			if(other == null)
				return false;
			if(!(other instanceof TestArrayClass))
				return false;
			TestArrayClass o = (TestArrayClass)other;
			return this.demoValue == o.demoValue && this.demoStr.equals(o.demoStr);
		}
		
		@Override
		public String toString(){
			return this.demoValue + "@" + this.demoStr;
		}
	}
	
	public static TestArrayClass[] testvalue = new TestArrayClass[]{
		new TestArrayClass(1,"1"),
		new TestArrayClass(2,"o")
	};
}
