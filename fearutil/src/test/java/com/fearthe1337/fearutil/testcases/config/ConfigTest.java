package com.fearthe1337.fearutil.testcases.config;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.junit.Test;

import com.fearthe1337.fearutil.configuration.ConfigurationHandler;
import com.fearthe1337.fearutil.configuration.ConfigurationHandler.ConfigurationType;

public class ConfigTest {
	
	@Test
	public void primitiveTypeResolveTest() throws IOException{
		try{
		File f = new File("target/test-classes/test.yml");
		System.out.println(f.getCanonicalPath());
		assertTrue(f.isFile());
		FileConfiguration fc = YamlConfiguration.loadConfiguration(f);
		Class<ConfigObject> cfg = ConfigObject.class;
		ConfigurationType type = ConfigurationType.BUKKIT;
		ConfigurationHandler<ConfigObject> acfg = ConfigurationHandler.<ConfigObject>getHandler(type, cfg, fc);
		ConfigObject config = (ConfigObject) acfg.load();
		assertTrue(config.testString.equalsIgnoreCase("TestValue"));
		System.out.println("LEN=" + config.keys.length);
		for(Object o : config.keys){
			System.out.println(o);
		}
		
		assertArrayEquals(ConfigObject.testvalue, config.keys);
		
		File fSave = new File("target/test-classes/savetest.yml");
		acfg.save(config, (Object)fSave);
		//ConfigurationHandler<ConfigObject>.getHandler(type, cfg , fc);
		}catch(Throwable t){
			System.err.println(t);
			t.printStackTrace();
			throw new RuntimeException(t);
		}
	}
}
