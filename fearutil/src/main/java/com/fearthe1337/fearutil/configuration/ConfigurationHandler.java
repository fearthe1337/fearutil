package com.fearthe1337.fearutil.configuration;

import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;

import com.fearthe1337.fearutil.impl.BukkitConfigurationHandler;

public abstract class ConfigurationHandler<U extends AutoConfiguration> {
	public enum ConfigurationType{
		BUKKIT
	};
	
	public abstract U load();
	public abstract void save(U configuration, Object handler) throws IOException;
	
	public static <T extends AutoConfiguration> ConfigurationHandler<T> getHandler(ConfigurationType type, Class<T> clazz, Object config){
		return getHandler(type,clazz,config,(Object[])null);
	}
	
	public static <T extends AutoConfiguration> ConfigurationHandler<T> getHandler(ConfigurationType type, Class<T> clazz, Object config, Object ... constructorParms){
		switch(type){
			case BUKKIT:
				return new BukkitConfigurationHandler<T>(clazz,(FileConfiguration)config,constructorParms);
			default:
				throw new RuntimeException("Unknown configuration type: " + type);
		}
	}
}
