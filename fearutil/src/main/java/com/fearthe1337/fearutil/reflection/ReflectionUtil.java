package com.fearthe1337.fearutil.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

public class ReflectionUtil {
	
	public static List<Field> getFields(Class<?> clazz, Class<? extends Annotation> annotation){
		LinkedList<Field> list = new LinkedList<Field>();
		for(Class<?> cl = clazz; cl != null; cl = cl.getSuperclass()){
			for(Field f : cl.getDeclaredFields()){
				System.out.println("Checking field: " + f.getName());
				if(f.isAnnotationPresent(annotation)){
					System.out.println("Annotation is present.");
					list.add(f);
				}else{
					
				}
			}
		}
		return list;
	}
	
	public static void setField(Field field, Object object, Object value){
		try{
			if(!field.isAccessible()){
				field.setAccessible(true);
				field.set(object, value);
				field.setAccessible(false);
			}else{
				field.set(object, value);
			}
		}catch(Throwable t){
			throw new RuntimeException(t);
		}
	}
	
	public static Object getField(Field field, Object object){
		try{
			Object value = null;
			if(!field.isAccessible()){
				field.setAccessible(true);
				value = field.get(object);
				field.setAccessible(false);
			}else{
				value = field.get(object);
			}
			return value;
		}catch(Throwable t){
			throw new RuntimeException(t);
		}
	}
	
	public static Class<?> translatePrimitive(Class<?> clazz){
		if(clazz == Byte.TYPE){
			return Byte.class;
		}else if(clazz == Short.TYPE){
			return Short.class;
		}else if(clazz == Integer.TYPE){
			return Integer.class;
		}else if(clazz == Long.TYPE){
			return Long.class;
		}else if(clazz == Float.TYPE){
			return Float.class;
		}else if(clazz == Double.TYPE){
			return Double.class;
		}else if(clazz == Boolean.TYPE){
			return Boolean.class;
		}else if(clazz == Character.TYPE){
			return Character.class;
		}
		if(clazz.isPrimitive()){
			throw new RuntimeException("Unknown primitive type: " + clazz.getName());
		}
		return null;
	}
	
	public static boolean primitiveMatch(Class<?> left, Class<?> right){
		if(right.isPrimitive() && !left.isPrimitive()){
			return primitiveMatch(right,left);
		}
		
		if(!left.isPrimitive()){
			return false;
		}
		
		if(left.isPrimitive() && right.isPrimitive()){
			return left.equals(right);
		}
		
		Class<?> testClazz = translatePrimitive(left);
		return testClazz.isAssignableFrom(right);
	}
	
	@SuppressWarnings("unchecked")
	public static <X> Constructor<X> resolveConstructor(Class<?> clazz, Object[] args){
		for(Constructor<?> constructor : clazz.getConstructors()){
			Class<?>[] parms = constructor.getParameterTypes();
			if(args == null && (parms == null || parms.length == 0)){
				return (Constructor<X>)constructor;
			}
			
			if(args != null && parms != null){
				if(args.length != parms.length){
					continue;
				}
				
				boolean match = true;
				for(int i=0; match && (i < args.length); i++){
					if(args[i] == null)
						continue; //NULL can be any type.
					Class<?> requested = parms[i];
					Class<?> supplied = args[i].getClass();
					
					if(!requested.isAssignableFrom(supplied)){
						if(!primitiveMatch(requested,supplied)){
							match = false;
						}
					}
				}
				if(match){
					return (Constructor<X>)constructor;
				}
			}
		}
		return null;
	}
}
