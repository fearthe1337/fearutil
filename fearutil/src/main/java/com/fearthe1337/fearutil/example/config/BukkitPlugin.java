package com.fearthe1337.fearutil.example.config;

import java.io.IOException;

import org.bukkit.plugin.java.JavaPlugin;

import com.fearthe1337.fearutil.configuration.AutoConfiguration;
import com.fearthe1337.fearutil.configuration.ConfigValue;
import com.fearthe1337.fearutil.configuration.ConfigurationHandler;
import com.fearthe1337.fearutil.configuration.ConfigurationHandler.ConfigurationType;

public class BukkitPlugin extends JavaPlugin{
	private ConfigurationHandler<BukkitConfiguration> configHandler;
	private BukkitConfiguration config;
	
	public void onEnable(){
		Class<BukkitConfiguration> configClass = BukkitConfiguration.class;
		ConfigurationType configType = ConfigurationType.BUKKIT;
		 
		 
		this.configHandler = ConfigurationHandler.<BukkitConfiguration>getHandler(configType, configClass, this.getConfig());
		this.config = this.configHandler.load();
	}
	
	public void reloadConfig(){
		super.reloadConfig();
		if(this.configHandler != null){
			this.config = this.configHandler.load();
		}
	}
	
	public void saveConfig(){
		try{
			this.configHandler.save(config, super.getConfig());
			super.saveConfig();
		}catch(IOException exception){
			System.out.println("Error saving configuration: " + exception);
			exception.printStackTrace();
		}
	}
	
	
	static class BukkitConfiguration extends AutoConfiguration{
		@ConfigValue(key = "tax")
		private double tax;
		@ConfigValue(key = "min")
		private double minTax;
		@ConfigValue(key = "maxTransactionAmount")
		private double maxTransactionAmount;
		public BukkitConfiguration(){
			
		}
		@Override
		public void validate() {
			
		}
	}

}
