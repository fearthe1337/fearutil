package com.fearthe1337.fearutil.checks;

public class ArgumentCheck {
	public static void notNULL(Object o){
		if(o == null)
			throw new IllegalArgumentException();
	}
	
	public static void notNULL(Object o, String message){
		if(o == null)
			throw new IllegalArgumentException(message);
	}
}
