package com.fearthe1337.fearutil.configuration;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value = RetentionPolicy.RUNTIME)
public @interface ConfigurationObject {
	boolean readonly = false;
}
