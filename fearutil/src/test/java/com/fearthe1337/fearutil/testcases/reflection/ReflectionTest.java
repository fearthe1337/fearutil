package com.fearthe1337.fearutil.testcases.reflection;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;

import org.junit.Test;

import com.fearthe1337.fearutil.reflection.ReflectionUtil;

public class ReflectionTest {
	
	@Test
	public void primitiveTypeResolveTest(){
		assertTrue(Byte.class.equals(ReflectionUtil.translatePrimitive(Byte.TYPE)));
		assertTrue(Short.class.equals(ReflectionUtil.translatePrimitive(Short.TYPE)));
		assertTrue(Integer.class.equals(ReflectionUtil.translatePrimitive(Integer.TYPE)));
		assertTrue(Long.class.equals(ReflectionUtil.translatePrimitive(Long.TYPE)));
		assertTrue(Float.class.equals(ReflectionUtil.translatePrimitive(Float.TYPE)));
		assertTrue(Double.class.equals(ReflectionUtil.translatePrimitive(Double.TYPE)));
		assertTrue(Character.class.equals(ReflectionUtil.translatePrimitive(Character.TYPE)));
		assertTrue(Boolean.class.equals(ReflectionUtil.translatePrimitive(Boolean.TYPE)));
	}

	@Test
	public void testResolveConstructor() {
		//Tests for constructor's with no arguments.
		Constructor<?> con;
		con = ReflectionUtil.resolveConstructor(TestClassNoConstructorParms.class, null);
		assertNotNull(con);
		con = ReflectionUtil.resolveConstructor(TestClassNoConstructorParms.class, new Object[]{});
		
		//Tests for classes with no constructor or private ones.
		con = ReflectionUtil.resolveConstructor(TestClassNoConstructor.class, null);
		assertNull(con);
		con = ReflectionUtil.resolveConstructor(TestClassNoConstructor.class, new Object[]{});
		assertNull(con);
		con = ReflectionUtil.resolveConstructor(TestClassNoConstructor.class, new String[]{});
		assertNull(con);
		con = ReflectionUtil.resolveConstructor(TestClassNoConstructor.class, new String[]{"JUnit"});
		assertNull(con);
		
		//Test for classes with arguments and with more then one argument and ones with more then 1 constructor.
		con = ReflectionUtil.resolveConstructor(TestClassMultiConstructor.class,null);
		assertNotNull(con);
		assertTrue(con.getParameterTypes().length == 0);		
		con = ReflectionUtil.resolveConstructor(TestClassMultiConstructor.class,new Object[]{});
		assertNotNull(con);
		assertTrue(con.getParameterTypes().length == 0);
		con = ReflectionUtil.resolveConstructor(TestClassMultiConstructor.class,new Object[]{10});
		assertNotNull(con);
		con = ReflectionUtil.resolveConstructor(TestClassMultiConstructor.class,new Object[]{new Object()});
		assertNotNull(con);
		assertTrue(con.getParameterTypes().length == 1);
		assertTrue(con.getParameterTypes()[0].equals(Object.class));
		con = ReflectionUtil.resolveConstructor(TestClassMultiConstructor.class,new Object[]{"JUnit"});
		assertNotNull(con);
		assertTrue(con.getParameterTypes().length == 1);
		assertTrue(con.getParameterTypes()[0].equals(Object.class));
		con = ReflectionUtil.resolveConstructor(TestClassMultiConstructor.class,new Object[]{10,new Object()});
		assertNotNull(con);
		assertTrue(con.getParameterTypes().length == 2);
		assertTrue(con.getParameterTypes()[0].equals(Integer.TYPE));
		assertTrue(con.getParameterTypes()[1].equals(Object.class));
		con = ReflectionUtil.resolveConstructor(TestClassMultiConstructor.class,new Object[]{new Object(),10});
		assertNull(con);
		con = ReflectionUtil.resolveConstructor(TestClassMultiConstructor.class,new Object[]{10,"JUnit"});
		assertNotNull(con);
		assertTrue(con.getParameterTypes().length == 2);
		assertTrue(con.getParameterTypes()[0].equals(Integer.TYPE));
		assertTrue(con.getParameterTypes()[1].equals(Object.class));
	}
	
	
	
	public static class TestClassNoConstructorParms{
		public  TestClassNoConstructorParms(){
		}
	}
	
	public static class TestClassNoConstructor{
		private  TestClassNoConstructor(){
		}
	}
	
	public static class TestClassMultiConstructor{
		public TestClassMultiConstructor(){
			
		}
		
		public TestClassMultiConstructor(Object o){
			
		}
		
		public TestClassMultiConstructor(int o, Object o2){
			
		}
	}
}
